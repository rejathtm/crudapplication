package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.BookModel;
import com.example.service.BookService;

@RestController
public class BookController {
	@Autowired
	BookService bookService;
	
	@GetMapping("/repo")
	private List<BookModel> getFromRepo() {
		return bookService.getAllBooks();
	}
	
	@PostMapping("/repo")
	private String postToRepo(@RequestBody BookModel book) {
		bookService.addToRepo(book);
		return book.getName();
	}
	
	@GetMapping("/get")
	private List<BookModel> getTest() {
		return bookService.getBooks();
	}
	
	@PostMapping("/add")
	private String postTest(@RequestBody BookModel book) {
		bookService.addBook(book);
		return book.getName();
	}
	
	@PutMapping("/update")
	private String putTest(@RequestBody BookModel book) {
		bookService.changeBookName(book);
		return book.getName();
	}
	
	@DeleteMapping("/delete/{name}")
	private int deleteTest(@PathVariable("name") String name) {
		int index=bookService.deleteBook(name);
		return index;
	}
}