package com.example.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.model.BookModel;
@Repository
public interface BookRepository extends CrudRepository<BookModel, Integer>{
	
}
