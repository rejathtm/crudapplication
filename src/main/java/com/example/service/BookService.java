package com.example.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.BookModel;
import com.example.repository.BookRepository;

@Service
public class BookService {
	@Autowired
	BookRepository bookRepository;
	public List<BookModel> bookList=new ArrayList<>();
	
//	add new book to repository
	public void addToRepo(BookModel books) {
		bookRepository.save(books);
	}
	
//	get book from repository
	public List<BookModel> getAllBooks(){
		List<BookModel> books= new ArrayList<>();
		bookRepository.findAll().forEach(book->books.add(book));
		return books;
	}

//	adding new book entry
	public void addBook(BookModel book) {
		bookList.add(book);
	}
	
//	getting list of all books
	public List<BookModel> getBooks(){
		return(bookList);
	}


	
//	change the name of book
	public void changeBookName(BookModel book) {
		for(int i =0;i<bookList.size();i++) {
			if(book.getId()==bookList.get(i).getId()) {
				bookList.set(i,book);break;		
			}
		}
	}
	
//	delete the book
	public int deleteBook(String name) {
		for(int i =0;i<bookList.size();i++) {
			
			if(bookList.get(i).getName().equals(name)) {
				int id=bookList.get(i).getId();
				bookList.remove(i);	
				return(id);
			}
		}
		return(-1);
	}
}